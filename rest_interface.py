import datetime

from flask import Flask, request, json, render_template, url_for
from flask_cors import CORS

from src.price_calculator import calculate_prices
from src.registrator import register_stations, initialize, set_consumption
from src.model import retriever

application = Flask(__name__)
CORS(application)


@application.route("/")
def hello():
    return "<h1 style='color:blue'>Hello Chargers!</h1>"

# Get URL to receive all charging stations relevant for calculation. Manipulate charging station objects by
# adding attribute "price" to json data.
@application.route("/get_stations", methods={"POST"})
def get_stations():
    req_data = request.get_json()
    station_ids = req_data['stations']
    register_stations(station_ids)
    current = datetime.datetime.utcnow()
    date = datetime.datetime(year=current.year, month=current.month, day=current.day, hour=current.hour)
    return json.dumps(calculate_prices(station_ids, date.hour))


# Get the consumption over the last 24 hours
@application.route("/get_consumption", methods={"GET"})
def get_consumption():
    return json.dumps(retriever.get_consumption(int(request.values.get('stationId'))))


# Interface for the smart-meters to send their data
@application.route("/report_consumption", methods={"GET"})
def report_consumption():
    station_id = int(request.values.get('stationId'))
    watt = float(request.values.get('watt'))
    set_consumption(station_id, datetime.datetime.utcnow(), watt)
    return "<h1 style='color:blue'>Data sent sucessfully</h1>"


# Initialize the database
@application.route("/populate", methods=["GET"])
def populate():
    initialize()
    return "<h1 style='color:blue'>Populated database</h1>"


if __name__ == "__main__":
    application.run(host='0.0.0.0')
