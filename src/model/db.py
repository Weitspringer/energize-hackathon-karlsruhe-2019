import sqlite3


# Initialize local database operated with sqlite 3
def init_database():
    connection = sqlite3.connect("local.db")
    cursor = connection.cursor()

    sql_command = """DROP TABLE IF EXISTS stations;"""
    cursor.execute(sql_command)
    sql_command = """DROP TABLE IF EXISTS consumptions;"""
    cursor.execute(sql_command)
    sql_command = """
    CREATE TABLE stations ( 
    station_id INTEGER PRIMARY KEY);"""
    cursor.execute(sql_command)
    sql_command = """
        CREATE TABLE consumptions ( 
        station_id INTEGER,
        timestamp TIMESTAMP,
        watt FLOAT,
        UNIQUE(station_id, timestamp),
        FOREIGN KEY(station_id) REFERENCES stations(station_id));"""
    cursor.execute(sql_command)

    connection.commit()
    connection.close()
    return


def register_station(station_id):
    if isinstance(station_id, int):
        connection = sqlite3.connect("local.db")
        cursor = connection.cursor()
        sql_command = """
            INSERT OR IGNORE INTO
            stations(station_id)
            VALUES({});""".format(station_id)
        cursor.execute(sql_command)
        connection.commit()
        connection.close()
    else:
        return


def set_consumption(station_id, timestamp, watt):
    if isinstance(station_id, int):
        connection = sqlite3.connect("local.db")
        cursor = connection.cursor()
        if not isinstance(watt, float):
            watt = 0.0
        sql_command = """
                    INSERT OR REPLACE INTO
                    consumptions(station_id, timestamp, watt)
                    VALUES({},"{}",{});""".format(station_id, timestamp, watt)
        cursor.execute(sql_command)
        connection.commit()
        connection.close()
    else:
        return


def get_consumption(station_id):
    if isinstance(station_id, int):
        connection = sqlite3.connect("local.db")
        cursor = connection.cursor()

        sql_command = """SELECT timestamp, watt FROM consumptions WHERE station_id={}""".format(station_id)

        cursor.execute(sql_command)
        watts_per_hour = cursor.fetchall()
        connection.close()
        return watts_per_hour
