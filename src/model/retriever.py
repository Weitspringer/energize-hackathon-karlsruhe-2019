from . import db


def get_consumption(station_id):
    return dict(db.get_consumption(station_id))
