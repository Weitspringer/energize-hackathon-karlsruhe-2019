import random

p1 = [0.36, 0.37, 0.50, 0.51, 0.47, 0.55, 0.30, 0.17, 0.19, 0.10, 0.07, 0.10, 0.70, 0.11, 0.20, 0.14, 0.21, 0.22, 0.15,
      0.20, 0.24, 0.28, 0.31, 0.27]
p2 = [0.35, 0.41, 0.48, 0.54, 0.52, 0.52, 0.34, 0.13, 0.14, 0.11, 0.03, 0.03, 0.03, 0.18, 0.17, 0.14, 0.24, 0.30, 0.20,
      0.24, 0.23, 0.26, 0.25, 0.27]
p3 = [0.33, 0.41, 0.44, 0.54, 0.45, 0.51, 0.36, 0.15, 0.20, 0.15, 0.04, 0.06, 0.03, 0.12, 0.14, 0.09, 0.25, 0.28, 0.19,
      0.17, 0.26, 0.25, 0.22, 0.31]
p4 = [0.33, 0.39, 0.47, 0.53, 0.47, 0.54, 0.37, 0.11, 0.13, 0.08, 0.12, 0.06, 0.03, 0.10, 0.14, 0.09, 0.24, 0.26, 0.16,
      0.21, 0.26, 0.29, 0.29, 0.31]
p5 = [0.33, 0.43, 0.47, 0.54, 0.46, 0.46, 0.34, 0.16, 0.18, 0.08, 0.11, 0.03, 0.04, 0.17, 0.19, 0.14, 0.30, 0.30, 0.20,
      0.18, 0.28, 0.24, 0.26, 0.25]
p6 = [0.36, 0.35, 0.47, 0.51, 0.53, 0.52, 0.39, 0.20, 0.16, 0.12, 0.09, 0.04, 0.08, 0.12, 0.11, 0.07, 0.29, 0.29, 0.21,
      0.24, 0.22, 0.23, 0.30, 0.34]


def calculate_prices(station_ids, hour):
    result = []
    for station_id in station_ids:
        solution_number = random.randint(1, 6)
        solution_array = []
        if solution_number == 1:
            solution_array = p1
        elif solution_number == 2:
            solution_array = p2
        elif solution_number == 3:
            solution_array = p3
        elif solution_number == 4:
            solution_array = p4
        elif solution_number == 5:
            solution_array = p5
        elif solution_number == 6:
            solution_array = p6

        key = station_id
        value = solution_array[hour]
        result.append((key, value))
    return dict(result)
