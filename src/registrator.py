import datetime

from .model import db

standard_array = [8.0, 9.0, 10.0, 10.0, 10.0, 7.0, 3.0, 3.0, 2.0, 1.5, 1.0, 1.3, 3.0, 3.0, 2.0, 5.0, 5.3, 3.5, 4.0,
                  4.7, 5.0, 5.2, 6.0, 7.5]
reference_array = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23]


def initialize():
    db.init_database()
    register_stations([1, 2, 3, 4, 5, 6])


def register_stations(station_ids):
    for station_id in station_ids:
        db.register_station(station_id)
        current = datetime.datetime.utcnow()
        date = datetime.datetime(year=current.year, month=current.month, day=current.day, hour=current.hour)
        hour = date.hour
        array = shift_array(hour, standard_array)
        t1 = datetime.timedelta(hours=1)
        for watt in reversed(array):
            set_consumption(station_id, date, watt)
            date = date - t1
    return


def set_consumption(station_id, timestamp, watt):
    db.set_consumption(station_id, timestamp, watt)
    return


def shift_array(key, array):
    return array[key:]+array[:key]
